bpg-web
======
This is the NodeJS based site for Brewpub Games, your favourite indie game development group.

[![Build Status](https://drone.io/bitbucket.org/brewpubgames/bpg-web/status.png)](https://drone.io/bitbucket.org/brewpubgames/bpg-web/latest)

### Client Dependencies ###
	- RequireJS 2.1.0
	- Modernizr 2.6.2
	- jQuery 2.0.2
	- Twitter Bootstrap 2.3.2
  
### Server-Side Dependencies
- See package.json

### GruntJS Commands ###
- test
- build
- build-prod