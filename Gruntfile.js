module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		less: {
			development : {
				yuicompress : false,
				files: {
					// dest : src
					'public/style/style.css': ['public/style/style.less']
				}
			},
			production: {
				files: {
					// dest : src
					'public/style/style.css': ['public/style/style.less']
				},
				yuicompress: true
			}
		},
		mochacli: {
			options: {
				reporter: 'nyan',
				bail: true
			},
			all: ['tests/*.js']
		}
	});
  
	// plugins
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-mocha-cli');
  
	// alias tasks
	grunt.registerTask('test', ['mochacli']);
	grunt.registerTask('build',['less:development', 'mochacli']);
	grunt.registerTask('build-prod',['less:production', 'mochacli']);
	
};