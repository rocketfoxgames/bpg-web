
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Home' });
};

exports.who = function(req, res) {
	res.render('who', { title: 'Who We Are'});
};

exports.play = function(req, res) {
	res.render('play', { title: 'Play Lovebucket'});
};