require.config({
	enforceDefine: true,
	paths: {
		modernizr : [
			'lib/modernizr'
		],
		jquery : [
			'http://code.jquery.com/jquery-2.0.2.min',
			'lib/jquery'
		]
	},
	shim: {
		'modernizr' : {
			exports: 'Modernizr'
		}
	}
});

define(['modernizr','jquery'], function(Modernizr, $) {
	// app code goes here
});